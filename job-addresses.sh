#!/usr/bin/env bash

#place this file in /etc/crond.daily with name job-addresses (no .sh)
#login to the registry

#get the latest version of address-api image
tag=`docker images --format "{{.Tag}}" --filter=reference='{your registry path}/servicetrips/back-end/address-api:*' | head -n 1`

image={your registry path}/servicetrips/back-end/address-api:$tag


docker run --rm -e ADDRESS_ELASTIC_HOST=$ADDRESS_ELASTIC_HOST -e ADDRESS_ELASTIC_PORT=$ADDRESS_ELASTIC_PORT -v /opt/addresses/extadress.txt:/opt/import.txt $image /usr/bin/php /var/www/bin/console servicetrips:store-addresses /opt/import.txt

