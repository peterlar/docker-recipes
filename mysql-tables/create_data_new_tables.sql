/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `cms_category`
--

LOCK
TABLES `cms_category` WRITE;
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
INSERT INTO `cms_category` (name)
VALUES ('Ansök om tillstånd'),
       ('Kontakta oss'),
       ('Webbplatsen'),
       ('Hjälp'),
       ('Informationstexter'),
       ('Felmeddelanden'),
       ('Jag vill bli ombud');
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;
UNLOCK
TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Dumping data for table `cms`
--

LOCK
TABLES `cms` WRITE;
INSERT INTO `cms`
VALUES (1, 7, 'Ansök om färdtjänsttillstånd', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/portal?uri=gbglnk%3a20160139272315', '', 1),
       (2, 7, 'Ansök om flexlinjetillstånd', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/portal?uri=gbglnk%3agbg.page.7dcd7e59-fb9e-49a6-b369-0cd96b5dfb21', '', 2),
       (3, 8, 'Kundservice', 'Intern sida', 'INTERN', '/contact',
        'Har du synpunkter eller frågor kring färdtjänst så kan du kontakta serviceresors kundservice.<div><br></div><div><b>Telefon</b></div><div><b><a href=\"tel:031-419552\">031-419552</a></b></div><div><br></div><div><b>Telefontider</b></div><div>Mån-fre: 8-12</div><div><br></div><div><b>E-post</b></div><div><a href=\"kundservice.serviceresor@trafikkontoret.goteborg.sekundservice.serviceresor@trafikkontoret.goteborg.se\">kundservice.serviceresor@trafikkontoret.goteborg.se</a><br></div><div><br></div><div><b>Postadress:</b></div><div>Box 30054, 400 43 Göteborg</div><div><br></div>',
        0),
       (4, 8, 'Boka resa via telefon', 'Intern sida', 'INTERN', '/contact',
        'Du kan alltid boka din resa via telefon. Samtalet kostar som ett vanligt lokalsamtal och tjänsten är öppen dygnet runt.<div><br></div><div><b>Färdtjänstbokning</b></div><div><b><a href=\"tel:031-41 95 42\">031-41 95 42</a></b></div><div><br></div><div><div><b>Flexlinjebokning</b></div><div><b><a href=\"tel:031-41 95 42\">031-41 95 42</a></b></div></div>',
        0),
       (5, 8, 'Trafikledningen', 'Intern sida', 'INTERN', '/contact',
        '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\">Är bilen försenad? <o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\">Om du har väntat mer än 10 minuter ska du ringa vår\ntrafikledning.<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\"><o:p>&nbsp;</o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\"><b>Telefon</b></span></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\"><b><a href=\"tel:031-41 95 45\">031-41 95 45</a></b></span></p>\n\n',
        0),
       (6, 8, 'Synpunkter', 'Intern sida', 'INTERN', '/contact',
        '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Hör av dig till vår kundservice så hjälper du oss att bli\nbättre.&nbsp;</span></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Självklart tar vi också gärna emot beröm om du är extra nöjd med något.<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\"><o:p>&nbsp;</o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\"><b>E-post</b><o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><a href=\"kundservice.serviceresor@trafikkontoret.goteborg.se\">kundservice.serviceresor@trafikkontoret.goteborg.se</a><br></p>',
        0),
       (7, 9, 'Om webbplatsen', 'Intern sida', 'INTERN', '/about',
        '<h4><span style=\"text-align: justify;\">Om Webbplatsen</span></h4><div><span style=\"text-align: justify;\">Här ska information in som beskriver webplatsen</span></div>',
        0),
       (8, 9, 'Webbplatskarta', 'Intern sida', 'INTERN', '/sitemap',
        '<h4><span style=\"text-align: justify;\">Webbplatskarta</span></h4><div><span style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ullamcorper malesuada enim quis iaculis. Donec porttitor magna et eros dignissim faucibus. Aenean vel dapibus lorem. Ut pulvinar rhoncus blandit. In enim nulla, convallis et pharetra eget, condimentum ac justo. Pellentesque eget felis ligula. Integer venenatis ipsum vehicula elit scelerisque, eu mattis libero pellentesque. Sed accumsan ullamcorper leo, quis tincidunt urna tincidunt tincidunt. Nulla eu sapien velit. Suspendisse potenti.</span></div>',
        0),
       (9, 9, 'Våra kakor', 'Intern sida', 'INTERN', '/cookies',
        '<h4><span style=\"text-align: justify;\">Våra kakor</span></h4><div><span style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ullamcorper malesuada enim quis iaculis. Donec porttitor magna et eros dignissim faucibus. Aenean vel dapibus lorem. Ut pulvinar rhoncus blandit. In enim nulla, convallis et pharetra eget, condimentum ac justo. Pellentesque eget felis ligula. Integer venenatis ipsum vehicula elit scelerisque, eu mattis libero pellentesque. Sed accumsan ullamcorper leo, quis tincidunt urna tincidunt tincidunt. Nulla eu sapien velit. Suspendisse potenti.</span></div>',
        0),
       (10, 9, 'Tillgänglighet', 'Intern sida', 'INTERN', '/availability',
        '<h4><span style=\"text-align: justify;\">Tillgänglighet</span></h4><div><span style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ullamcorper malesuada enim quis iaculis. Donec porttitor magna et eros dignissim faucibus. Aenean vel dapibus lorem. Ut pulvinar rhoncus blandit. In enim nulla, convallis et pharetra eget, condimentum ac justo. Pellentesque eget felis ligula. Integer venenatis ipsum vehicula elit scelerisque, eu mattis libero pellentesque. Sed accumsan ullamcorper leo, quis tincidunt urna tincidunt tincidunt. Nulla eu sapien velit. Suspendisse potenti.</span></div>',
        0),
       (11, 10, 'Frågor och svar', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/portal?uri=gbglnk%3agbg.page.da89aa01-9e7d-4800-b34a-c76fe7bb872a', '', 1),
       (12, 10, 'Hur blir jag ett ombud', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/portal?uri=gbglnk%3agbg.page.8fc05346-a308-436c-93bf-bd6686c69557#htoc-4', '', 2),
       (13, 10, 'Så fungerar tjänsten', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/myportal/start/fardtjanst/sa-fungerar-fardtjansten', '', 3),
       (14, 10, 'Så fungerar flexlinjen', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/myportal/start/buss-sparvagn-tag/flexlinjen/sa-fungerar-flexlinjen', '', 4),
       (15, 10, 'Försenat fordon och resegaranti', 'Extern länk', 'EXTERN',
        'https://goteborg.se/wps/portal?uri=gbglnk%3agbg.page.ecbb79b2-ff5b-4001-b10f-b1470e24991d', '', 5),
       (16, 10, 'Användarvillkor', 'Extern länk', 'EXTERN', 'http://www.google.se', '', 6),
       (17, 11, 'Inlogg felmeddelande', 'Informationsikon', 'INTERN', '#', 'Lorem ipsum', 0),
       (18, 11, 'Skapa konto', 'Informationsikon', 'INTERN', '#',
        '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Om du har ett tillstånd för serviceresor så kan du skapa\nett konto. Då kan du själv se dina bokade resor, boka nya resor och avboka\nresor. Skapa även ett konto om du vill bli ombud för en resenär. Det är samma inloggningsuppgifter\ntill både hemsida och appen.<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\"><o:p>&nbsp;</o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Det enklaste sättet att skapa konto på Serviceresan är\natt använda Mobilt BankID. Då behöver du bara skriva in ditt personnummer och\nsedan verifiera din identitet på appen Mobillt BankID på din mobila enhet. Om\ndu inte har Mobilt BankID, kan du beställa Mobilt BankID via din\ninternetbank.</span><span lang=\"en-SE\"><o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\"><o:p>&nbsp;</o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Du kan även skapa konto med lösenord. Du kan då ange ditt\npersonnummer och sedan beställa hem ett lösenord med posten. Detta för att\nsäkerställa din identitet. Du kan sedan logga in med det startlösenord som står\ni välkomstbrevet.<font face=\"Calibri Light, sans-serif\"><span style=\"font-size: 10pt;\"><o:p></o:p></span></font></span></p>',
        0),
       (19, 11, 'På plats i tid', 'Informationsikon', 'INTERN', '#',
        '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Fordonet kan vara fördröjt upp till 10 minuter utan att\ndet räknas som försenat.<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span lang=\"SV\" style=\"\">Fordonet väntar max fem minuter på plats.<font face=\"Calibri Light, sans-serif\"><span style=\"font-size: 10pt;\"><o:p></o:p></span></font></span></p>',
        0),
       (20, 11, 'Tillval och hjälpmedel', 'Informationsikon', 'INTERN', '#',
        '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\">Det går inte att lägga mer än 2 bagage om du bokar via\nappen.&nbsp;</span></p><p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><span style=\"\" lang=\"SV\">Ring istället <a href=\"tel:031-419545\">031-419545</a> för dina bokningar.<font face=\"Calibri Light, sans-serif\"><span style=\"font-size: 10pt;\"><o:p></o:p></span></font></span></p>',
        0),
       (21, 11, 'Instruktioner till föraren', 'Informationsikon', 'INTERN', '#', 'Här behöver vi ha en text', 0),
       (22, 11, 'Månadskostnad', 'Informationsikon', 'INTERN', '#',
        '<p class=\"MsoNormal\"><span lang=\"SV\" style=\"line-height: 104%;\">Faktura informationen uppdateras en gång per dag.&nbsp;<br></span><span style=\"color: inherit; font-family: inherit; font-size: 1rem;\">Det är alltid beloppet på\nfakturan som gäller.</span></p>',
        0),
       (23, 11, 'Antal resor', 'Informationsikon', 'INTERN', '#',
        '<p class=\"MsoNormal\"><span lang=\"SV\" style=\"line-height: 104%;\">Information om hur många resor som har utförts uppdateras en gång per dag.<font face=\"Calibri Light, sans-serif\"><span style=\"font-size: 10pt;\"><o:p></o:p></span></font></span></p>',
        0),
       (24, 11, 'Tillstånd', 'Informationsikon', 'INTERN', '#',
        '<p class=\"MsoNormal\"><span lang=\"SV\" style=\"line-height: 104%;\">Här kan du se de tillstånd som du är beviljad. För mer information om\ntillstånd kan du ringa din tillståndshandläggare på telefon: 031-419590<font face=\"Calibri Light, sans-serif\"><span style=\"font-size: 10pt;\"><o:p></o:p></span></font></span></p>',
        0),
       (27, 12, '6005', 'Felmeddelande', 'ERROR', '', 'No CP available for this SID.<br>', 0),
       (28, 12, '6006', 'Felmeddelande', 'ERROR', '', 'Kan ej bokas i appen. Var god ring beställningscentral.<br>', 0),
       (29, 12, '1997', 'Felmeddelande', 'ERROR', '', 'Kan ej bokas i appen. Var god ring beställningscentral.', 0),
       (30, 12, '4609', 'Felmeddelande', 'ERROR', '', '<div>Det går ingen flex-trafik vid denna tid.<br></div>', 0),
       (116, 12, '6007', 'Felmeddelande', 'INTERN', '', 'Session data to old', 0),
       (117, 12, '206', 'Felmeddelande', 'INTERN', '', 'Turen är full, vad god boka en annan tid.', 0),
       (118, 12, '6008', 'Felmeddelande', 'INTERN', '', 'No SID available', 0),
       (119, 12, '6011', 'Felmeddelande', 'INTERN', '',
        'Beställningen hittades inte. Var god ring beställningscentral.', 0),
       (120, 12, '6016', 'Felmeddelande', 'INTERN', '',
        'Beställningen hittades inte. Var god ring beställningscentral.', 0),
       (121, 12, '30082', 'Felmeddelande', 'INTERN', '', 'Personnummer kunde inte hittas vid säker inloggning.', 0),
       (122, 12, '30089', 'Felmeddelande', 'INTERN', '', 'Ingen sådan klientbehörighet.', 0),
       (123, 12, '30090', 'Felmeddelande', 'INTERN', '', 'Det gick inte att få kundnummer för den här användaren.', 0),
       (124, 12, '100000', 'Felmeddelande', 'INTERN', '', 'Det gick inte att logga in. Var god att testa igen.', 0),
       (125, 12, '100001', 'Felmeddelande', 'INTERN', '', 'Något gick fel. Var god ring beställningscentral.', 0),
       (126, 12, '5', 'Felmeddelande', 'INTERN', '', 'Något gick fel. Var god ring beställningscentral.', 0),
       (127, 12, '10', 'Felmeddelande', 'INTERN', '', 'Objektet saknas.', 0),
       (128, 12, '11', 'Felmeddelande', 'INTERN', '', 'Du har angett ett ogiltligt värde.', 0),
       (129, 12, '12', 'Felmeddelande', 'INTERN', '', 'Det gick inte att skapa, vänligen se över informationen.', 0),
       (130, 12, '13', 'Felmeddelande', 'INTERN', '', 'Det gick inte att uppdatera, vänligen se över informationen.',
        0),
       (131, 12, '14', 'Felmeddelande', 'INTERN', '', 'Det gick inte att ta bort, vänligen prova igen senare.', 0),
       (132, 12, '20', 'Felmeddelande', 'INTERN', '', 'Gick inte att hitta användare/lösenordet stämmer inte', 0),
       (133, 12, '21', 'Felmeddelande', 'INTERN', '', 'Kopplingsfel, vänligen prova igen senare.', 0),
       (134, 12, '30', 'Felmeddelande', 'INTERN', '',
        'Finns ingen resenär/ombud med detta användarnamn eller ombud är ej tillgänglig.', 0),
       (138, 11, 'Datum har passerat', 'Validering', 'INTERN', '', 'Du kan inte välja ett äldre datum än dagens datum.',
        0),
       (139, 11, 'Datum för långt fram', 'Validering', 'INTERN', '',
        'Du kan inte välja ett datum som är äldre än 14 dagar.', 0),
       (140, 11, 'Tiden har passerat', 'Validering', 'INTERN', '', 'Du kan inte välja en tid som har passerat.', 0),
       (141, 11, 'Ditt tillstånd har slutat gälla', 'Validering', 'INTERN', '',
        'Du har inget giltigt tillstånd, ditt tillstånd slutar gälla \n[yyyy-mm-dd]. Ändra datumet för resan till före [yyyy-mm-dd]&nbsp;för att \nkunna boka.',
        0),
       (142, 11, 'Ditt tillstånd har inte börjat gälla', 'Validering', 'INTERN', '',
        'Du har inget giltigt tillstånd, ditt tillstånd börjar gälla \n[yyyy-mm-dd]. Ändra datumet för resan till efter [yyyy-mm-dd]&nbsp;för att \nkunna boka. ',
        0),
       (143, 11, 'Tid för nära inpå', 'Validering', 'INTERN', '',
        'Din bokning måste vara 2 timmar framåt. Ring <a href=\"tel:031-41 95 45\">031-41 95 45</a> för att boka resan om du måste åka inom 2 timmar. ',
        0),
       (144, 11, 'Tid för nära inpå en annan resa', 'Validering', 'INTERN', '',
        'Det finns redan en resa inom 30 minuter för din önskade tid. Din avresetid måste skilja sig minst 30 minuter på dina resor.',
        0),
       (145, 11, 'Går inte att boka på detta datum', 'Validering', 'INTERN', '',
        'Det går inte att boka på detta datum. Ring <a href=\"tel:031-41 95 45 \">031-41 95 45 </a>för att boka resan. ',
        0),
       (146, 11, 'Nytt lösenord', '', 'INTERN', '',
        'Din begäran på nytt lösenord har skickats. Det nya lösenordet skickas till din folkbokföringsadress.<br>', 0),
       (147, 11, 'Din session har gått ut', '', 'INTERN', '',
        'Din session har gått ut. Var god att logga in igen. <br>', 0),
       (148, 11, 'Det går inte att boka', 'Intern sida', 'INTERN', '',
        'Det går inte att boka. Ring <a href=\"tel:031-41 95 45\">031-41 95 45</a> för att boka resan.<br>', 0),
       (149, 11, 'Order går inte att avboka', 'Intern sida', 'INTERN', '',
        'Din order går inte att avboka. Ring&nbsp;<a href=\"tel:031-41 95 45\">031-41 95 45</a> för att avboka resan.<br>',
        0),
       (150, 11, 'Skapa konto', 'Intern sida', 'INTERN', '',
        'Din begäran har skickats. Ett kontobrev skickas till din folkbokföringsadress.<br>', 0),
       (151, 13, 'Jag vill bli ombud', 'Extern l�nk', 'EXTERN',
        'https://goteborg.se/wps/portal?uri=gbglnk:gbg.page.8fc05346-a308-436c-93bf-bd6686c69557#htoc-4', '', 0),
       (156, 12, '15', 'Felmeddelande', 'ERROR', '', 'Anv�ndaren finns redan.', 0),
       (157, 11, 'L�senord uppdaterats', 'Ditt l�senord har uppdaterats', 'INTERN', '',
        '<p>Ditt l�senord har uppdaterats<br>�</p>', 0),
       (158, 12, '30007', 'Felmeddelande', 'ERROR', '', '<p>Passet �r slut. Resan tas d�rf�r inte bort.</p>', 0),
       (159, 12, '40', 'Felmeddelande', 'ERROR', '', '<p>Adress saknas, var god och fyll i adress.</p>', 0),
       (160, 11, 'Max antal resor', 'Max antal resor', 'INTERN', '', '<p>Max antal resor har uppn�tts</p>', 0),
       (162, 11, 'Ombudsf�rfr�gan', 'Skicka ombudsf�rfr�gan', 'INTERN', '',
        '<p>Din beg�ran om ombudsf�rfr�gan har skickats.</p>', 0);
UNLOCK
TABLES;

--
-- Dumping data for table `driver_instructions`
--

LOCK
TABLES `driver_instructions` WRITE;
/*!40000 ALTER TABLE `driver_instructions` DISABLE KEYS */;
INSERT INTO `driver_instructions`
VALUES (1, 5, 'Bärhjälp'),
       (2, 3, 'Inga medresenärer'),
       (3, 7, 'Max 2 medresenärer'),
       (4, 10, 'Ingen hiss'),
       (5, 1, 'Extra bagage');
INSERT INTO `driver_instructions`
VALUES (12, 7, 'Extra bagage'),
       (13, 5, 'Lämna matpåsar');
/*!40000 ALTER TABLE `driver_instructions` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Dumping data for table `information_travel_types`
--

LOCK
TABLES `information_travel_types` WRITE;
/*!40000 ALTER TABLE `information_travel_types` DISABLE KEYS */;
INSERT INTO `information_travel_types`
VALUES (1, 1, 1),
       (2, 1, 2),
       (3, 1, 3),
       (4, 2, 1),
       (5, 2, 2),
       (6, 2, 4);
/*!40000 ALTER TABLE `information_travel_types` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Dumping data for table `travel_types`
--

LOCK TABLES `travel_types` WRITE;
/*!40000 ALTER TABLE `travel_types`
    DISABLE KEYS */;
INSERT INTO `travel_types`
VALUES (1, 'Färdtjänst PB', 'Färdtjänst', 'FTJSKÖ', 'FTJ', 'taxi', 'Skövde', '1', '1'),
       (2, 'Färdtjänst PB', 'Färdtjänst', 'FTJUDD', 'FTJ', 'taxi', 'Uddevalla', '1', '1'),
       (3, 'Färdtjänst PB', 'Färdtjänst', 'FTJVÄN', 'FTJ', 'taxi', 'Vänersborg', '1', '1'),
       (4, 'Färdtjänst SPC', 'Färdtjänst Specialfordon', 'SPCGÖT', 'SPC', 'buss', 'Götene', '1', '1'),
       (5, 'Skolresa', 'Skolresa', 'SKOUDD', 'SKO', 'buss', 'Uddevalla', '0', '1'),
       (6, 'Färdtjänst SPC', 'Färdtjänst Specialfordon', 'SPCUDD', 'SPC', 'buss', 'Uddevalla', '1', '1'),
       (7, 'Sjukresa', 'Sjukresa', 'LAMED', 'SJUK', 'taxi', '', '1', '1'),
       (8, 'Arbetsresa SPC', 'Arbetsresa Specialfordon', 'ARSHÄR', 'ARB', 'buss', 'Härryda', '1', '1'),
       (9, 'Färdtjänst SPC', 'Färdtjänst Specialfordon', 'SPCHÄR', 'SPC', 'buss', 'Härryda', '1', '1'),
       (10, 'Arbetsresa PB', 'Arbetsresa', 'ARBBEN', 'ARB', 'buss', 'Bengtsfors', '1', '1'),
       (11, 'Färdtjänst SPC', 'Färdtjänst Specialfordon', 'SPCGÖT', 'SPC', 'buss', 'Götene', '1', '1'),
       (12, 'Kom iland', 'Kom iland', 'KILGÖT', 'KIL', 'flex', 'Götene', '0', '1'),
       (13, 'Färdtjänst PB', 'Färdtjänst', 'FTJGÖT', 'FTJ', 'taxi', 'Götene', '1', '1'),
       (14, 'Arbetsresa SPC', 'Arbetsresa Specialfordon', 'ARSGÖT', 'ARB', 'buss', 'Götene', '1', '1'),
       (15, 'Riksfärdtjänst', 'Riksfärdtjänst', 'RIKHÄR', 'RIK', 'buss', 'Härryda', '0', '0'),
       (16, 'Färdtjänst SPC', 'Färdtjänst Specialfordon', 'SPCTID', 'SPC', 'buss', 'Tidaholm', '1', '1'),
       (17, 'Färdtjänst PB', 'Färdtjänst', 'FTJTID', 'FTJ', 'taxi', 'Tidaholm', '1', '1'),
       (18, 'Färdtjänst PB', 'Färdtjänst', 'FTJTÖR', 'FTJ', 'taxi', 'Töreboda', '1', '1'),
       (19, 'Arbetsresa PB', 'Arbetsresa', 'ARBHÄR', 'ARB', 'taxi', 'Härryda', '1', '1'),
       (20, 'Färdtjänst PB', 'Färdtjänst', 'FTJKUÄ', 'FTJ', 'taxi', 'Kungälv', '1', '1'),
       (21, 'Färdtjänst SPC', 'Färdtjänst Specialfordon', 'SPCKUÄ', 'SPC', 'buss', 'Kungälv', '1', '1'),
       (22, 'Arbetsresa Kungälv', 'Arbetsresa', 'ARBKUÄ', 'ARB', 'taxi', 'Kungälv', 1, 1);
/*!40000 ALTER TABLE `travel_types`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES 
  (1,'manage_admins','Manage Admins','2015-01-12 15:56:19','2015-01-12 15:56:19'),
  (2,'manage_users','Manage Users','2015-01-12 15:56:19','2015-01-12 15:56:19'),
  (3,'manage_settings','Manage Settings','2015-01-12 15:56:19','2015-01-12 15:56:19'),
  (4,'manage_information','Manage Information','2015-01-12 15:56:19','2015-01-12 15:56:19');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES
  (1,'Admin','Administratörshantering','2015-01-12 15:56:19','2015-01-12 15:56:19'),
  (2,'UsersAdmin','Användarhantering','2015-01-12 15:56:19','2015-01-12 15:56:19'),
  (3,'SettingsAdmin','Inställningar','2015-01-12 15:56:19','2015-01-12 15:56:19'),
  (4,'InformationAdmin','Information','2015-01-12 15:56:19','2015-01-12 15:56:19');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,2,2),(6,3,3),(7,4,4);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
