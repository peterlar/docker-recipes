-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: ratten
-- ------------------------------------------------------
-- Server version       5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES 
(2,'order_minimum_minutes_between_trips','Minimum tid mellan resor i minuter','30','2015-01-12 15:28:24','2020-04-29 13:47:25'),
(3,'order_minimum_minutes_until_arrival','Minimum tid i framtiden i minuter för frammetid','120','2015-01-12 15:28:24','2015-02-02 11:47:16'),
(4,'order_unbookable_dates','Datum som ej går att boka på, anges i MMDD','1224,1225,1226,1231,0101,0619,0709,0810','2015-01-12 15:28:24','2020-08-04 21:29:42'),
(9,'function_invoice_info','Om funktionen för att visa information inför nästa faktura skall vara aktiv i klienten','1','2015-01-12 15:28:24','2015-04-16 10:59:03'),
(15,'warn_when_nbr_of_trips_remains','Gränsen för när klienten skall varna för att resorna håller på att ta slut','10','2015-01-12 15:28:24','2015-01-29 13:36:33'),
(17,'app_inactive_logout','Tid i sekunder för hur länge användaren kan vara inaktiv i APPen innan dess att användaren loggas ut','1200','2015-01-12 15:28:24','2020-08-14 09:49:58'),
(21,'aids_which_cannot_be_combined','Hjälpmedel som inte kan kombineras med andra hjälpmedel. ','I,S,A','2015-01-20 08:54:28','2017-08-11 08:43:50'),
(22,'accountMinimumAccountAgeBeforeDelete','Hur många dagar gammalt ett konto måste vara innan det kan tas bort av kontokontrollsjobbet','880','2015-01-26 12:31:22','2016-06-02 12:50:47'),
(23,'accountMaxDaysFromLastLogin','Hur många dagar en konto ska vara oanvänt (dvs att man inte loggat) in innan det är kan tas bort.','900','2015-01-26 12:31:52','2016-03-31 13:58:12'),
(24,'max_nbr_of_co_travellers_municipality','Max antal medresenärer per baserat på vilken kundgrupp/kommun som användaren tillhör. Data ska vara i formatet Kommun:värde,Kommun:värde. Exempelvis Göteborg:3,Härryda:2','Ale:3,Alingsås:1,Bengtsfors:1,Bollebygd:1,Dals Ed:1,Essunga:1,Falköping:1,Färgelanda:1,Grästorp:1,Gullspång:1,Götene:1,Herrljunga:1,Hjo:1,Karlsborg:1,Kungälv:2,Lerum:3,Lidköping:1,Lilla Edet:1,Lysekil:1,Mariestad:1,Mark:1,Mellerud:1,Munkedal:1,Orust:1,Partille:3,Skara:1,Skövde:1,Sotenäs:1,Stenungsund:1,Strömstad:3,Svenljunga:3,Tanum:1,Tibro:1,Tidaholm:1,Tjörn:1,Trollhättan:3,Töreboda:1,Uddevalla:1,Vara:1,Vårgårda:3,Vänersborg:1,Åmål:3,Öckerö:1','2015-01-26 17:25:05','2020-09-15 10:14:42'),
(30,'position_maximum_minutes_after_arrival','Maximalt antal minuter efter estimatedTime som användaren kan spåra sitt fordon','10','2016-01-22 11:22:57','2020-10-21 11:46:07'),
(31,'position_maximum_minutes_before_arrival','Anger hur lång tid innan estimatedTime som användaren kan spåra sitt fordon','10','2016-01-22 11:23:11','2020-10-30 08:06:01'),
(32,'position_update_intervall','intervall som anger hur ofta appen skall hämta positionsdata från ratten. värdet är i sekunder','15','2016-01-22 11:23:44','2016-01-22 11:23:44'),
(33,'app_warning_before_logout','Tid i sekunder före logout som användaren får en varning','40','2016-01-22 11:23:44','2016-01-22 11:23:44');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-18 11:25:42
